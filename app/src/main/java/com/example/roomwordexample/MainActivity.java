package com.example.roomwordexample;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.roomwordexample.adapter.WordAdapter;
import com.example.roomwordexample.entity.Word;
import com.example.roomwordexample.viewmodel.WordViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnLongPressedInterface {

    static final int NEW_WORD_REQUEST_CODE = 1;

    RecyclerView recyclerView;
    WordViewModel wordViewModel;
    WordAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivityForResult(intent, NEW_WORD_REQUEST_CODE);
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        adapter = new WordAdapter(this,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(List<Word> words) {
                adapter.setWords(words);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NEW_WORD_REQUEST_CODE && resultCode == RESULT_OK){
            Word word = new Word(data.getStringExtra(NewWordActivity.REPLY_WORD));
            wordViewModel.insert(word);
        }else {
            Toast.makeText(MainActivity.this, "Word is empty", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.delete_all:
                wordViewModel.deleteAll();
                break;
            case R.id.descending:
                wordViewModel.getAllWordsDesc().observe(this, new Observer<List<Word>>() {
                    @Override
                    public void onChanged(List<Word> words) {
                        adapter.setWords(words);
                    }
                });
                break;
            case R.id.ascending:
                wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
                    @Override
                    public void onChanged(List<Word> words) {
                        adapter.setWords(words);
                    }
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLongPressed(final Word word) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        adb.setMessage("Are you sure you want to delete this word").setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                wordViewModel.deleteWord(word);
                dialog.dismiss();
            }
        })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();

    }
}
