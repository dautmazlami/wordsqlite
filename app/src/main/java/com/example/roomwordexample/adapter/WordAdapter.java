package com.example.roomwordexample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.roomwordexample.OnLongPressedInterface;
import com.example.roomwordexample.R;
import com.example.roomwordexample.entity.Word;

import java.util.List;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordViewHolder>{

    LayoutInflater inflater;
    List<Word> wordList;
    OnLongPressedInterface pressedListener;

    public WordAdapter(Context context,OnLongPressedInterface pressedListener){
        this.pressedListener = pressedListener;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recyclerview_item, parent, false);
        return new WordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WordViewHolder holder, int position) {
        if (wordList != null){
            Word word = wordList.get(position);
            holder.textView.setText(word.getWord());
        }else {
            holder.textView.setText("no word");
        }
    }

    @Override
    public int getItemCount() {
        if (wordList != null){
            return wordList.size();
        }
        return 0;
    }

    public void setWords(List<Word> words){
        wordList = words;
        notifyDataSetChanged();
    }

    public Word getWord(int position){
        Word word = wordList.get(position);
        return word;
    }

    class WordViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public WordViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            textView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    pressedListener.onLongPressed(getWord(getAdapterPosition()));
                    return false;
                }
            });
        }
    }
}
