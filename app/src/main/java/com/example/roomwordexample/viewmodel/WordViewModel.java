package com.example.roomwordexample.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.roomwordexample.entity.Word;
import com.example.roomwordexample.repository.WordRepository;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository wordRepository;
    private LiveData<List<Word>> allWords;
    private LiveData<List<Word>> allWordsDesc;

    public WordViewModel(Application application) {
        super(application);
        wordRepository = new WordRepository(application);
        allWords = wordRepository.getAllWords();
        allWordsDesc = wordRepository.getAllWordsDesc();
    }

    public void insert(Word word){
        wordRepository.insert(word);
    }

    public void deleteAll(){
        wordRepository.deleteAll();
    }

    public void deleteWord(Word word){
        wordRepository.deleteWord(word);
    }

    public LiveData<List<Word>> getAllWords(){
        return allWords;
    }

    public  LiveData<List<Word>> getAllWordsDesc(){
        return allWordsDesc;
    }
}
