package com.example.roomwordexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewWordActivity extends AppCompatActivity {

    EditText editText;
    Button button;
    public static final String REPLY_WORD = "Reply_Word";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);

        editText = findViewById(R.id.new_word_edit_text);
        button = findViewById(R.id.save_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(editText.getText().toString())){
                    setResult(RESULT_CANCELED, replyIntent);
                }else {
                    String word = editText.getText().toString();
                    replyIntent.putExtra(REPLY_WORD, word);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });

    }
}
