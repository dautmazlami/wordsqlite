package com.example.roomwordexample.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;

import com.example.roomwordexample.dao.WordDao;
import com.example.roomwordexample.database.WordRoomDatabase;
import com.example.roomwordexample.entity.Word;

import java.util.List;

public class WordRepository {

    private WordDao wordDao;
    private LiveData<List<Word>> allWords;
    private LiveData<List<Word>> allWordsDesc;

    public WordRepository(Application application){
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        wordDao = db.wordDao();
        allWords = wordDao.getAllWords();
        allWordsDesc = wordDao.getAllWordsDesc();
    }

    public LiveData<List<Word>> getAllWords(){
        return allWordsDesc;
    }

    public LiveData<List<Word>> getAllWordsDesc(){
        return allWords;
    }

    public void insert(Word word){
        new InsertAsyncTask(wordDao).execute(word);
    }

    public void deleteAll(){
        new DeleteAllAsyncTask(wordDao).execute();
    }

    public void deleteWord(Word word){
        new deleteWordAsyncTask(wordDao).execute(word);
    }

    private static class InsertAsyncTask extends AsyncTask <Word, Void, Void>{
        private WordDao asyncTaskDao;

        InsertAsyncTask(WordDao dao){
            this.asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            asyncTaskDao.insert(words[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncTask extends AsyncTask<Word,Void,Void>{
        private WordDao deleteAllAsyncTaskDao;

        DeleteAllAsyncTask(WordDao dao){
            this.deleteAllAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            deleteAllAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteWordAsyncTask extends AsyncTask<Word,Void,Void>{
        private WordDao deleteAsyncTaskDao;

        deleteWordAsyncTask(WordDao dao){
            this.deleteAsyncTaskDao = dao;

        }

        @Override
        protected Void doInBackground(Word... words) {
            deleteAsyncTaskDao.deleteWord(words[0]);
            return null;
        }
    }

}
