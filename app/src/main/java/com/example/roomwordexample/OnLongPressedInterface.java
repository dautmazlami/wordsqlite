package com.example.roomwordexample;

import com.example.roomwordexample.entity.Word;

public interface OnLongPressedInterface {

    public void onLongPressed(Word word);


}
